let allEtudiants = [{id: 1, nom: "Dupont", prenom:"Pierre", date:"1998-07-07", numCI:4545521}];
window.onload = () => {
    listeEtudiants();
}
function valideForm () {
    const etudiant = {
            id: allEtudiants[allEtudiants.length - 1].id + 1,
            nom: document.forms["formulaire"]["nom"].value,
            prenom: document.forms["formulaire"]["prenom"].value,
            date: document.forms["formulaire"]["date"].value,
            numCI: document.forms["formulaire"]["num"].value,
    }  
    allEtudiants.push(etudiant);          
    listeEtudiants();
}


function listeEtudiants() {
    document.getElementById("tableau").innerHTML="";

   // newRows(allEtudiants.length);   
    allEtudiants.forEach((etudiant) =>{
        const newColumns = {
            nom: document.createElement('td'),
            prenom: document.createElement('td'),
            date: document.createElement('td'),
            numCI: document.createElement('td'),
            age : document.createElement('td'),
            modifier: document.createElement('td'),
            supprimer: document.createElement('td')

        };
        const newRows = document.createElement('tr');
        const newButtons = {   
            modifier: document.createElement('button'),
            supprimer: document.createElement('button'),

        };
        for(const [nomColonne, tdTag] of Object.entries(newColumns)) {
            newRows.appendChild(tdTag).innerHTML = "";
            if (nomColonne == 'modifier') {
                newButtons.modifier.setAttribute('value', nomColonne)
                newButtons.modifier.innerHTML = "" + nomColonne;
                tdTag.appendChild(newButtons.modifier)}
            if (nomColonne == 'supprimer') {
                newButtons.supprimer.setAttribute('value', nomColonne)
                newButtons.supprimer.innerHTML = "" + nomColonne;
                tdTag.appendChild(newButtons.supprimer)}
            tdTag.setAttribute('id', nomColonne + "Etudiants" + etudiant.id)
            document.getElementById("tableau").appendChild(newRows);

           if ( nomColonne === "nom" ){ newRows.appendChild(tdTag).innerHTML = "" + etudiant.nom;}
           if ( nomColonne === "prenom" ){ newRows.appendChild(tdTag).innerHTML = "" + etudiant.prenom;}
           if ( nomColonne === "date" ){ newRows.appendChild(tdTag).innerHTML = "" + etudiant.date;}
           if ( nomColonne === "numCI" ){ newRows.appendChild(tdTag).innerHTML = "" + etudiant.numCI;}
           if ( nomColonne === "age" ){ newRows.appendChild(tdTag).innerHTML = "" + calculAge(etudiant.date);}
        }
      
        for(const [key, value] of Object.entries(newButtons)) {
            value.setAttribute('type', 'button');
            value.setAttribute('class', key);
            value.setAttribute('id', etudiant.id);
            value.setAttribute('value', key);
            document.getElementById("tableau").appendChild(newRows);

        }
    })

    modifOuSuppEtudiant();
    }


function modifOuSuppEtudiant() {
        const suppButton = document.querySelectorAll(".supprimer");
        const modifButton = document.querySelectorAll(".modifier");

        suppButton.forEach((button) => {
            button.addEventListener('click', () => suppEtudiant(button.id))
        })

        modifButton.forEach((button) => {
            button.addEventListener('click', () => modifEtudiant(button.id))
        })

    }

function suppEtudiant(id) {
    allEtudiants.forEach((etudiant) => {
        const posEtudiantinArray = allEtudiants.indexOf(etudiant)
        if (etudiant.id === parseInt(id)){ allEtudiants.splice(posEtudiantinArray, 1)};
    })
    listeEtudiants();
}

function modifEtudiant(id) {
   
    document.getElementById("modifier").style.visibility = "visible";
               
    allEtudiants.forEach((etudiant) => {
        if (etudiant.id === parseInt(id)){ 
                document.getElementById('inputID').value = "" + etudiant.id,
                document.getElementById('inputNom').value="" + etudiant.nom,
                document.getElementById('inputPrenom').value="" + etudiant.prenom,
                document.getElementById('inputDate').value="" + etudiant.date,
                document.getElementById('inputnumCI').value="" + etudiant.numCI 
        };
    })
    
}

function modifierEtudiant() {
    const newDatas = {
        id: document.forms["formulaire"]["ID"].value,
        nom: document.forms["formulaire"]["nom"].value,
        prenom: document.forms["formulaire"]["prenom"].value,
        date: document.forms["formulaire"]["date"].value,
        numCI: document.forms["formulaire"]["num"].value,
    };
        
        document.getElementById('nomEtudiants' + newDatas.id).innerHTML = "" + newDatas.nom,
        document.getElementById('prenomEtudiants' + newDatas.id).innerHTML = "" + newDatas.prenom,
        document.getElementById('dateEtudiants' + newDatas.id).innerHTML = "" + newDatas.date,
        document.getElementById('numCIEtudiants' + newDatas.id).innerHTML = "" + newDatas.numCI
        document.getElementById("modifier").style.visibility = "hidden";
        const etudModif =  allEtudiants.findIndex(etudiant => etudiant.id == newDatas.id)   
        allEtudiants.splice(etudModif, 1, newDatas);
}


    function calculAge(date) {
        var date = Array.from(date);
        date = date[5] + date[6] + "/" + date [8] + date [9]+ "/"+ date[0]+ date[1]+ date[2]
        + date[3]
        date = new Date(date);
        var month_diff = Date.now() -date.getTime();
        var age_dt = new Date(month_diff);
        var year = age_dt.getUTCFullYear();
        var age =  Math.abs(year - 1970);
        return(age);
    }


    function sortTable(n) {
        var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
        table = document.getElementById("myTable");
        switching = true;
        
        dir = "asc";
       
        while (switching) {
          
          switching = false;
          rows = table.rows;
          
          for (i = 1; i < (rows.length - 1); i++) {
           
            shouldSwitch = false;
           
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];
            
            if (dir == "asc") {
              if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
               
                shouldSwitch = true;
                break;
              }
            } else if (dir == "desc") {
              if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
              
                shouldSwitch = true;
                break;
              }
            }
          }
          if (shouldSwitch) {
         
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
           
            switchcount ++;
          } else {
            
            if (switchcount == 0 && dir == "asc") {
              dir = "desc";
              switching = true;
            }
        }
    }
}